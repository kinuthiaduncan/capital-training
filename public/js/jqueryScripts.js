$(document).ready(function() {
    // $('#calendar').fullCalendar({
    //     dayClick: function(date, jsEvent, view) {
    //         // $(this).css('background-color', 'rgba(69, 186, 1, 1)');
    //         document.getElementById("selected-date").innerHTML = date.format('MMMM ' + 'DD,' +' YYYY');
    //         $('#time-select').show();
    //         $("#date-input").val(date.format());
    //         $('#select_time').focus();
    //         // alert('yaay'+date.format());
    //     }
    // });
    // $('#select_time').timepicker({
    //     timeFormat: 'h:mm p',
    //     interval: 60,
    //     minTime: '10',
    //     maxTime: '5:00pm',
    //     defaultTime: '11',
    //     startTime: '10:00',
    //     dynamic: false,
    //     dropdown: true,
    //     scrollbar: true
    // });

    var fixmeTop = $('.course-nav').offset().top;
    $(window).scroll(function() {
        var currentScroll = $(window).scrollTop();
        if (currentScroll >= fixmeTop) {
            $('.course-nav').css({
                position: 'fixed',
                top: '0',
                left: '0',
                'min-width': '100%',
                'z-index': '1',
            });

            $('.footer-fixed').css({
                position: 'fixed',
                bottom: '0',
                left: '0',
                'min-width': '100%',
                'z-index': '1',
            });

        } else {
            $('.course-nav').css({
                position: 'static'
            });
             $('.footer-fixed').css({
                position: 'static'
            });
        }
    });

});