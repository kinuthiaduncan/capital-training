<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('/');

Route::get('/landing-page', function (){
   return view('landing');
});

Auth::routes();

Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/courses','Course\CoursesController@course')->name('courses');

Route::get('/training-courses/{slug}', 'Course\CoursesController@index')->name('training-courses');

Route::get('/enroll/{slug}', 'Course\EnrollController@index');
Route::get('/six-sigma/enroll/{slug}', 'Course\EnrollController@sigmaEnroll');
Route::post('/enroll/register', 'Course\EnrollController@store')->name('register-user');
Route::post('/pay', 'Course\EnrollController@pay')->name('pay');

//Route::get('/training-courses/capm', 'CAPM\CAPMController@index')->name('training-courses/capm');
//Route::get('/training-courses/pmp', 'PMP\PMPController@index')->name('training-courses/pmp');
//Route::get('/training-courses/agile', 'Agile\AgileController@index')->name('training-courses/agile');
//Route::get('/training-courses/six-sigma', 'Sigma\SigmaController@index')->name('training-courses/six-sigma');

Route::get('/about', 'AboutController@index')->name('about');
Route::get('/faqs', 'FAQController@index')->name('faqs');
Route::get('/student-satisfaction-policy', 'SatisfactionPolicyController@index')->name('student-satisfaction');
Route::post('/course-search', 'Course\CoursesController@search')->name('course-search');
Route::post('/contact', 'AboutController@contact')->name('contact');
Route::get('/sigma/{slug}','Course\CoursesController@show');
//Route::get('/booking', 'Booking\BookingController@index')->name('booking');
//Route::get('/booking/calendar', 'Booking\BookingController@getCalendar')->name('calendar');
//Route::post('/book/details', 'Booking\BookingController@inputDetails')->name('booking-details');
//Route::post('/book/store', 'Booking\BookingController@store')->name('booking-store');
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth', 'csrf'], function () {

    Route::resource('user', 'Admin\UserController', [
        'names' => [
            'index' => 'user.index',
            'create' => 'user.create',
            'store' => 'user.store',
            'edit' => 'user.edit',
            'update' => 'user.update',
            'show' => 'user.show'
        ]]);

    Route::get('/user/{id}/delete','Admin\UserController@delete' );

    Route::resource('payments', 'Admin\PaymentController', [
        'names' => [
            'index' => 'payment.index',
            'store' => 'payment.store',
            'edit' => 'payment.edit',
            'update' => 'payment.update',
            'show' => 'payment.show'
        ]]);
    Route::post('/api/get/payments', 'Admin\PaymentController@getPayments');
    Route::get('/payments/{id}/create', 'Admin\PaymentController@create');
    Route::get('/payments/{id}/delete', 'Admin\PaymentController@delete');
    Route::resource('trainers', 'Admin\TrainerController', [
        'names' => [
            'index' => 'trainer.index',
            'store' => 'trainer.store',
            'edit' => 'trainer.edit',
            'update' => 'trainer.update',
            'show' => 'trainer.show'
        ]]);
    Route::post('/api/get/trainers', 'Admin\TrainerController@getTrainers');
});