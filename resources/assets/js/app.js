
require('./bootstrap');

$(document).foundation();

Vue.component('payments' , require('./components/Payments.vue'));
Vue.component('trainers' , require('./components/Trainers.vue'));

const app = new Vue({
    el: '#app',
    data: function () {
        return {
            'course':'',
            'status':'',
            'trainer': ''
        }
    },
    methods:{
        getPayments() {
            Event.$emit('filtering_payments', {
                course: this.course,
                status: this.status
            });
        },
        getTrainers() {
            Event.$emit('filtering_trainers', {
                trainer: this.trainer,
            });
        },
    }
});
