<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Capital Training Academy</title>

    <!-- Styles -->
    <title>Capital Training Academy</title>

    <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

</head>
<body>
<div class="row landing-main">
    <div class=" small-12 large-12 columns landing-title">
        {{--<h3>Capital Training Academy</h3>--}}
        <img src="/assets/img/CTA_logo.png" class="landing_logo" >
        <br/>
        <a href="{{route('about')}}" class="button contact-btn">CONTACT US</a>
    </div>
</div>
</body>
<!-- Scripts -->
<script src="{{mix('/js/app.js')}}"></script>

<script>
    $(document).foundation();

</script>
</html>
