@extends('layouts.app')

@section('content')
    <div class="row" style="padding:0;min-width: 100%;">

        <div class="small-12 large-12 columns">

            <div class="row course-header">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row">
                        <div class="small-12 large-12 columns course-title-div" style="height: 10vh">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="small-12 large-12 columns" id="course-advisor">
            <div class="row">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row" style="padding-bottom: 2%;">
                        <div class="small-12 large-12 columns">
                            <div class="row">
                                <div class="small-12 large-3 columns sigma-courses">
                                    <div class="">
                                        <i class="fa fa-cloud" style="color: #ccc; font-size: 80px; margin-bottom: 40px;"></i>
                                        <h5>CAPM</h5>
                                        {{--<p class="price">$599</p>--}}
                                        <p>Fundamental Concepts</p>
                                        <p>8 Hours Classroom</p>
                                        <p>Designed as Overview</p>
                                        <a href="{{route('training-courses','capm')}}" class="button sigma-btn">FIND A CLASS</a>
                                    </div>
                                </div>
                                <div class="small-12 large-3 columns sigma-courses-popular">
                                    <div class="">
                                        <i class="fa fa-globe" style="color: #fff; font-size: 80px; margin-bottom: 40px;"></i>
                                        <h5>SIX SIGMA</h5>
                                        {{--<p class="price">$1099</p>--}}
                                        <p>Greater Comprehension</p>
                                        <p>2 Day Classroom</p>
                                        <p>No Pre-Requisites</p>
                                        <a href="{{route('training-courses','sigma')}}" class="button sigma-btn">FIND A CLASS</a>
                                    </div>
                                </div>
                                <div class="small-12 large-3 columns sigma-courses">
                                    <div class="">
                                        <i class="fa fa-check-circle" style="color: #ccc; font-size: 80px; margin-bottom: 40px;"></i>
                                        <h5>PMP</h5>
                                        {{--<p class="price">$3899</p>--}}
                                        <p>Advanced Concepts</p>
                                        <p>5 Days Classroom</p>
                                        <p>Global Certification</p>
                                        <a href="{{route('training-courses','pmp')}}" class="button sigma-btn">FIND A CLASS</a>
                                    </div>
                                </div>
                                <div class="small-12 large-3 columns sigma-courses">
                                    <div class="">
                                        <i class="fa fa-thumbs-o-up" style="color: #ccc; font-size: 80px; margin-bottom: 40px;"></i>
                                        <h5>AGILE</h5>
                                        {{--<p class="price">$7899</p>--}}
                                        <p>Concept Mastery</p>
                                        <p>10 Days Classroom</p>
                                        <p>Top Global Certification</p>
                                        <a href="{{route('training-courses','agile')}}" class="button sigma-btn">FIND A CLASS</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection