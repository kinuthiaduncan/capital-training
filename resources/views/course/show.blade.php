@extends('layouts.app')

@section('content')
    <div class="row" style="padding:0;min-width: 100%;">

        <div class="small-12 large-12 columns">

            <div class="row course-header">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row">
                        <div class="small-12 large-12 columns course-title-div">
                            <h2>{!! $course->course_name !!}</h2>
                            <p>{!! $course->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="small-12 large-12 columns course-nav">
            <div class="row course-row">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 0 5%;">
                    <div class="row">
                        <div class="small-12 large-12 columns course-tabs">
                            @include('layouts.partials.course-tabs')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="small-12 large-12 columns" id="course-advisor">
            <div class="row">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row" style="padding-bottom: 2%;">
                        <div class="small-12 large-12 columns">
                            <h5 style="font-size: 1.2rem;font-weight: 600;">Course Advisor</h5>
                        </div>
                        <div class="small-12 large-2 columns">
                            <div class="staff-img">
                                <img data-type="image"
                                     src="{!! $trainer->photo !!}"
                                     style="width: 150px; height: 154px; object-fit: cover;">
                            </div>
                        </div>
                        <div class="small-12 large-8 columns">
                            <h6 style="font-size: 1.2rem;font-weight: 500;">{!! $trainer->first_name !!} {!! $trainer->last_name !!}</h6>
                            <p><b>{!! $trainer->title !!}</b></p>
                            <p>{!! $trainer->details !!}</p>
                        </div>
                        <div class="small-12 large-2 columns">
                            <h6 style="font-size: 1.2rem;font-weight: 600;">Accredited By:</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 large-8 columns">
                            <div class="row" style="border: solid #D3D3D3 1px; width: 100%;height: 100%;">
                                <div class="small-12 large-12 columns" style="text-align: center;">
                                    <h5 style="padding-top: 2%">Remote</h5>
                                </div>
                                @foreach($individual_courses as $individual_course)
                                <div class="small-12 large-6 columns" style="position: relative">
                                    <div style="border-bottom: solid #D3D3D3 1px; width: 100%;">
                                        <h6>{!! $individual_course->title !!}</h6>
                                    </div>
                                    <ul>
                                        {!! $individual_course->description !!}
                                    </ul>
                                    <div class="spacer"></div>
                                    <div class="course-action">
                                        <p>$ {!! $individual_course->price !!}</p>
                                        <a href="{{url('/enroll/'.$individual_course->slug)}}" class="button btn-course">ENROLL NOW</a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="small-12 large-4 columns">
                            <div class="row" style="border: solid #D3D3D3 1px;width: 100%;">
                                <div class="small-12 large-12 columns" style="text-align: center; position: relative;">
                                    <h5 style="padding-top: 2%">Physical</h5>
                                    <div style="border-bottom: solid #D3D3D3 1px; width: 100%;">
                                        <h6>{!! $business_course->title !!}</h6>
                                    </div>
                                    <ul style="text-align: left;">
                                        {!! $business_course->description !!}
                                    </ul>
                                    <div class="spacer"></div>
                                    <div class="course-action">
                                        <p>$ {!! $business_course->price !!}</p>
                                        <a href="{{url('/enroll/'.$business_course->slug)}}" class="button btn-course">ENROLL NOW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="min-width: 100%;">
        <div class="small-12 large-10 large-offset-1 columns" style="padding: 1% 4%;">
            <div class="row">
                <div class="small-12 large-8 columns">
                    <div class="row" id="features">
                        <div class="small-12 large-12 columns">
                            <h5>Key Features</h5>
                            <ul>
                            @foreach($course_features as $feature)
                                {!! $feature->feature !!}
                            @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="description" style="padding: 1% 0;">
                        <div class="small-12 large-12 columns">
                            <h5>Course Description</h5>
                        </div>
                        <div class="small-12 large-12 columns">
                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                @foreach($description as $item)
                                <li class="accordion-item " data-accordion-item>
                                    <a href="#" class="accordion-title">{!! $item->question !!}</a>
                                    <div class="accordion-content" data-tab-content >
                                        <p>{!! $item->answer !!}</p>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="certification" style="padding: 1% 0;">
                        <div class="small-12 large-12 columns">
                            <h5>Exam and Certification</h5>
                        </div>
                        <div class="small-12 large-12 columns">
                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                @foreach($certifications as $certification)
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">{!! $certification->question !!}</a>
                                    <div class="accordion-content" data-tab-content >
                                        <p>{!! $certification->answer !!}</p>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="faq" style="padding: 1% 0;">
                        <div class="small-12 large-12 columns">
                            <h5>FAQs</h5>
                        </div>
                        <div class="small-12 large-12 columns">
                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                @foreach($faqs as $faq)
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">{!! $faq->question !!}</a>
                                    <div class="accordion-content" data-tab-content >
                                        <p>{!! $faq->answer !!}</p>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="small-12 large-4 columns side-bar">
                    <div class="show-contact">
                            <h5>Contact Us</h5>
                            <span>+12 34 543567</span>
                            <span class="phone_no"><i class="fa fa-phone"></i></span>
                        </div>

                    <div class="show-form">
                        <h5 style="text-align: center">Reach out to Us</h5>
                        {!! Form::open(['route'=>['contact']]) !!}
                        {!! Form::text('name',null,['placeholder'=>'Name...', 'required']) !!}
                        {!! Form::email('email',null,['placeholder'=>'Email...','required']) !!}
                        {!! Form::text('subject',null,['placeholder'=>'Subject...', 'required']) !!}
                        {!! Form::textarea('message',null,['placeholder'=>'Message...','rows'=>'1', 'required']) !!}
                        {!! Form::submit('CONTACT US', ['class'=>'button']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection