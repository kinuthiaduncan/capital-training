@extends('layouts.app')

@section('content')
    <div class="row" style="padding: 2%">
        <div class="small-12 large-10 large-offset-1 columns">
            <div class="row">
                @if($course_details)
                <div class="small-12 large-12 columns course-detail">
                    <h2>{!! $course_details->course->course_name !!} {!! $course_details->location->location_name !!}</h2>
                </div>
                <div class="small-12 large-12 columns course-description">
                    <p>{!! $course_details->description !!}</p>
                </div>
                <div class="small-12 large-12 columns" style="padding: 0">
                    <img src="/assets/img/people-banner2.png" >
                </div>
                <div class="small-12 large-12 columns course-info">
                    <p><b>Title:</b> {!! $course_details->course->course_name !!}</p>
                    <p><b>Duration:</b> {!! $course_details->duration !!}</p>
                    <p><b>Format:</b> {!! $course_details->format !!}</p>
                    <p><b>Time:</b> {!! \Carbon\Carbon::parse($course_details->start_time)->format('g:i A') !!}
                     to {!! \Carbon\Carbon::parse($course_details->end_time)->format('g:i A') !!}</p>
                    <p><b>PMI Approved Hours:</b> {!! $course_details->hours !!}</p>
                </div>
                @else
                    <h2>Sorry, we could not find the course</h2>
                @endif
            </div>
        </div>
    </div>
@endsection