@extends('layouts.app')

@section('content')
    <div class="row" style="padding: 2%">
        <div class="small-12 large-10 large-offset-1 columns">
            <div class="row">
                <div class="small-12 large-12 columns about-title">
                    <h2>ABOUT</h2>
                </div>
                <div class="small-12 large-12 columns about-text">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum."</p>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum."</p>
                </div>
                <div class="small-12 large-12 columns about-title">
                    <h2>STAFF</h2>
                </div>
                <div class="small-12 large-12 columns about-text">
                    <div class="row">
                        <div class="small-12 large-3 columns">
                            <div class="staff-img">
                                <img data-type="image"
                                     src="https://static.wixstatic.com/media/b2c0a7_0dfa9e2ad0112a8515045331b855b84d.png/v1/fill/w_180,h_180,al_c,usm_0.66_1.00_0.01/b2c0a7_0dfa9e2ad0112a8515045331b855b84d.png"
                                     style="width: 180px; height: 180px; object-fit: cover;">
                            </div>
                            <div class="staff-name">
                                <h4>Adriane Flynn</h4>
                                <p>Founder & CEO</p>
                            </div>
                            <div class="staff-desc">
                                <p>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text”
                                    or double click me to add your own content and make changes to the font.</p>
                            </div>
                        </div>
                        <div class="small-12 large-3 columns">
                            <div class="staff-img">
                                <img data-type="image"
                                     src="https://static.wixstatic.com/media/b2c0a7_5dd0ed2059b6b2763151c379f489a264.png/v1/fill/w_180,h_180,al_c,usm_0.66_1.00_0.01/b2c0a7_5dd0ed2059b6b2763151c379f489a264.png">
                            </div>
                            <div class="staff-name">
                                <h4>Sara Fischer</h4>
                                <p>Head of Marketing</p>
                            </div>
                            <div class="staff-desc">
                                <p>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text”
                                    or double click me to add your own content and make changes to the font.</p>
                            </div>
                        </div>
                        <div class="small-12 large-3 columns">
                            <div class="staff-img">
                                <img data-type="image"
                                     src="https://static.wixstatic.com/media/b2c0a7_a2ac0f1ced3af4185f2400d795de41bd.png/v1/fill/w_180,h_180,al_c,usm_0.66_1.00_0.01/b2c0a7_a2ac0f1ced3af4185f2400d795de41bd.png"
                                     style="width: 180px; height: 180px; object-fit: cover;">
                            </div>
                            <div class="staff-name">
                                <h4>Tom Portman</h4>
                                <p>Head of Advertisment</p>
                            </div>
                            <div class="staff-desc">
                                <p>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text”
                                    or double click me to add your own content and make changes to the font.</p>
                            </div>
                        </div>
                        <div class="small-12 large-3 columns">
                            <div class="staff-img">
                                <img data-type="image"
                                     src="https://static.wixstatic.com/media/b2c0a7_0dfa9e2ad0112a8515045331b855b84d.png/v1/fill/w_180,h_180,al_c,usm_0.66_1.00_0.01/b2c0a7_0dfa9e2ad0112a8515045331b855b84d.png"
                                     style="width: 180px; height: 180px; object-fit: cover;">
                            </div>
                            <div class="staff-name">
                                <h4>Adriane Flynn</h4>
                                <p>Founder & CEO</p>
                            </div>
                            <div class="staff-desc">
                                <p>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text”
                                    or double click me to add your own content and make changes to the font.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="small-12 large-12 columns about-title">
                    <h2>CONTACT US</h2>
                </div>
                <div class="small-12 large-12 columns about-text">
                    <div class="row">
                        <div class="small-12 large-6 columns contact-frm">
                            {!! Form::open(['route'=>['contact']]) !!}
                            {!! Form::text('name',null,['placeholder'=>'Your Name...', 'required']) !!}
                            {!! Form::email('email',null,['placeholder'=>'Your Email...', 'required']) !!}
                            {!! Form::text('subject',null,['placeholder'=>'Subject...', 'required']) !!}
                            {{ Form::textarea('message', null, ['size' => '30x5','required']) }}
                            {!! Form::submit('SUBMIT',['placeholder'=>'Message...','class'=>'button about-btn']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="small-12 large-6 columns contact-info">
                            <p><b>Tel:</b> 123-456-7890</p>
                            <p><b>Fax:</b> 123-456-7890</p>
                            <p>500 Terry Francois St. San Francisco, CA 94158</p>
                            <p><a href="mailto:info@capitaltraining.com">info@capitaltraining.com</a></p>
                        </div>
                        <div class="small-12 large-12 columns map">
                            <div id="googleMap" style="width:100%;height:300px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        function initMap() {
            var myLatLng = {lat: 37.7703706, lng: -122.3871226};

            var map = new google.maps.Map(document.getElementById('googleMap'), {
                zoom: 6,
                center: myLatLng
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Visit us here'
            });
        }
    </script>
@endsection