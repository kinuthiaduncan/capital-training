@extends('layouts.app')

@section('content')
<div class="row" style="padding: 2%">
    <div class="small-12 large-10 large-offset-1 columns">
        <div class="row">
            <div class="small-12 large-12 columns booking-title">
                <h5>Our Services</h5>
            </div>
            <div class="small-12 large-8 columns calendar-div">
                <div class="" id="calendar"></div>
            </div>
            <div class="small-12 large-4 columns meet-holder">
                <div class="lets-meet">
                    <div class="" style="border-bottom: solid black 1px">
                        <h5>Let's Meet</h5>
                        <p>1 Hour | Free Service</p>
                    </div>
                    <br/>
                    <div class="selected-date">
                        <p id="selected-date"></p>
                    </div>
                    <br/>
                    <div id="time-select" style="display: none">
                        <h6>Select the Time</h6>
                        {!! Form::open(['route'=>'booking-details']) !!}
                        {!! Form::hidden('date',null,['id'=>'date-input', 'required']) !!}
                        {!! Form::text('time','null',['id'=>'select_time','required']) !!}

                    <div class="booking-btns" style="text-align: center; padding-top:2%; width: 100%">
                        <br/>
                        {!! Form::submit('NEXT',['class'=>'button booking-btn']) !!}
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection