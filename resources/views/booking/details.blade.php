@extends('layouts.app')

@section('content')
    <div class="row" style="padding: 2%">
        <div class="small-12 large-10 large-offset-1 columns">
            <div class="row">
                <div class="small-12 large-12 columns booking-title">
                    <h5>Our Services</h5>
                </div>
                <div class="small-12 large-8 columns  book-details">
                    <h5>Add Your Info</h5>
                    <h6>Tell us a bit about yourself</h6>
                    <br />
                    {!! Form::open(['route'=>'booking-store']) !!}
                    {!! Form::hidden('date',$date) !!}
                    {!! Form::hidden('time',$time) !!}
                    {!! Form::label('name','Name*') !!}
                    {!! Form::text('name',null,['required']) !!}
                    {!! Form::label('email','Email*') !!}
                    {!! Form::email('email',null,['required']) !!}
                    {!! Form::label('phone','Phone Number') !!}
                    {!! Form::text('phone',null) !!}
                    {!! Form::label('message','Message') !!}
                    {!! Form::textarea('message',null,['size' => '30x5']) !!}
                </div>
                <div class="small-12 large-4 columns meet-holder">
                    <div class="lets-meet">
                        <div class="" style="border-bottom: solid black 1px">
                            <h5>Let's Meet</h5>
                            <p>1 Hour | Free Service</p>
                        </div>
                        <br/>
                        <div class="selected-date">
                            <p>{!! \Carbon\Carbon::parse($date)->format('l jS F Y') !!} {!! $time !!}</p>
                        </div>
                        <br/>
                        <div>

                            <div class="booking-btns" style="text-align: center; padding-top:2%; width: 100%">
                                <br/>
                                {!! Form::submit('BOOK IT',['class'=>'button booking-btn']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection