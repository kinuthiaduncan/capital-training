@extends('layouts.app')

@section('content')
    <div class="row" style="padding: 2%">
        <div class="small-12 large-10 large-offset-1 columns">
            <div class="row">
                <div class="small-10 large-10 columns booking-title">
                    <h5>Our Services</h5>
                </div>
                <div class="small-2 large-2 columns booking-login" style="text-align: right">
                    <p> @if (Auth::check())
                            <i class="fa fa-user-o" aria-hidden="true"></i> <a href="{{ url('/home') }}"> Hello</a> |
                            <a  href="/logout"> Sign Out</a>
                        @else
                            <i class="fa fa-user-o" aria-hidden="true"></i> <a href="{{ url('/login') }}">Login</a>
                        @endif
                    </p>
                </div>
            </div>
            <div class="row booking-intro">
                <div class="small-12 large-6 columns booking-pic">
                    <img src="/assets/img/booking.jpg">
                </div>
                <div class="small-12 large-6 columns booking-txt">
                    <div class="holder">
                        <h5>Let's Meet</h5>
                        <p>1 hr | Free Service</p>
                        <a href="{{route('calendar')}}" class="button book-btn">BOOK NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection