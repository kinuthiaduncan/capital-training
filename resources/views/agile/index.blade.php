@extends('layouts.app')

@section('content')
    <div class="row" style="padding:0;min-width: 100%;">
        <div class="small-12 large-12 columns">
            <div class="row course-header">
                 <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row">
                        <div class="small-12 large-12 columns course-title-div">
                        <h2>AGILE TRAINING COURSE</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="small-12 large-12 columns course-nav">
            <div class="row course-row">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 0 5%;">
                    <div class="row">
                        <div class="small-12 large-12 columns course-tabs">
                            @include('layouts.partials.course-tabs')
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="small-12 large-12 columns" id="course-advisor">
            <div class="row">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row" style="padding-bottom: 2%;">
                        <div class="small-12 large-12 columns">
                            <h5>Course Advisor</h5>
                        </div>
                        <div class="small-12 large-2 columns">
                            <div class="staff-img">
                                <img data-type="image"
                                     src="https://static.wixstatic.com/media/b2c0a7_0dfa9e2ad0112a8515045331b855b84d.png/v1/fill/w_180,h_180,al_c,usm_0.66_1.00_0.01/b2c0a7_0dfa9e2ad0112a8515045331b855b84d.png"
                                     style="width: 150px; height: 154px; object-fit: cover;">
                            </div>
                        </div>
                        <div class="small-12 large-8 columns">
                            <h5>Adriane Flynn</h5>
                            <h6>Founder, Business Consultant, Influential voice for ASQ</h6>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem</p>
                        </div>
                        <div class="small-12 large-2 columns">
                            <h5>Accredited By:</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 large-8 columns">
                            <div class="row" style="border: solid #D3D3D3 1px; width: 100%;height: 100%;">
                                <div class="small-12 large-12 columns" style="text-align: center;">
                                    <h5>For Individuals</h5>
                                </div>
                                    <div class="small-12 large-6 columns" style="position: relative">
                                        <div style="border-bottom: solid #D3D3D3 1px; width: 100%;">
                                            <h6>Self-Paced Learning</h6>
                                        </div>
                                        <ul>
                                            <li>180 days of access to high-quality, self-paced learning content designed by industry experts</li>
                                            <li>Lorem Ipsum</li>
                                        </ul>
                                        <div class="spacer"></div>
                                        <div class="course-action">
                                            <p>$ 100</p>
                                            <a href="" class="button btn-course">ENROLL NOW</a>
                                        </div> 
                                 </div>
                                <div class="small-12 large-6 columns" style="position: relative;">
                                    <div style="border-bottom: solid #D3D3D3 1px; width: 100%;">
                                        <h6>Online Classroom Remote Learning</h6>
                                    </div>
                                    <ul>
                                        <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem</li>
                                        <li> Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</li>
                                        <li>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue</li>
                                    </ul>
                                    <div class="spacer"></div>
                                    <div class="course-action">
                                             <p>$ 200</p>
                                            <a href="" class="button btn-course">ENROLL NOW</a>
                                        </div> 
                                </div>
                            </div>    
                        </div>
                        <div class="small-12 large-4 columns">
                            <div class="row" style="border: solid #D3D3D3 1px;width: 100%;">
                                <div class="small-12 large-12 columns" style="text-align: center; position: relative;">            
                                    <h5>Business</h5>
                                    <div style="border-bottom: solid #D3D3D3 1px; width: 100%;">
                                        <h6>Corporate training solutions</h6>
                                    </div>
                                        <ul style="text-align: left;">
                                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem</li>
                                            <li> Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</li>
                                            <li>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue</li>
                                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</li>
                                        </ul>
                                        <div class="spacer"></div>
                                        <div class="course-action">
                                             <a href="" class="button btn-course">CONTACT US</a>
                                        </div>                            
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            </div>
        </div>

        <!-- <div class="small-12 large-10 large-offset-1 columns">
            <div class="row course-header">
                <div class="small-12 large-12 columns course-title-div">
                    <h2>AGILE TRAINING COURSE</h2>
                </div>
            </div>
            <div class="row course-row">
                <div class="small-12 large-12 columns course-tabs">
                    {{--@include('layouts.partials.course-tabs')--}}
                </div>
            </div>
            <div class="row">
                <div class="course-tab-content" data-tabs-content="course-tabs">
                    <div class="small-12 large-12 columns tabs-panel is-active course-description" id="outline">
                    </div>
            </div>
        </div> -->


    </div>
    <div class="row" style="min-width: 100%;">
        <div class="small-12 large-10 large-offset-1 columns" style="padding: 1% 4%;">
            <div class="row">
                <div class="small-12 large-8 columns">
                    <div class="row" id="features">
                        <div class="small-12 large-12 columns">
                            <h5>Key Features</h5>
                            <ul>
                                <li>40 hours of instructor-led training</li>
                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                <li>27 hours of high quality e-learning content.</li>
                                <li>4 real life projects</li>
                                <li>4 simulation test papers</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="description" style="padding: 1% 0;">
                        <div class="small-12 large-12 columns">
                            <h5>Course Description</h5>
                        </div>
                        <div class="small-12 large-12 columns">
                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                <li class="accordion-item is-active" data-accordion-item>
                                    <a href="#" class="accordion-title">Why choose us for your PMP Boot Camp?</a>
                                    <div class="accordion-content" data-tab-content >
                                        <p>Our PMP Exam Prep Boot Camp is an accelerated, guaranteed path to achieving your PMP
                                            certification. We blend proven learning concepts with simple memorization techniques to make
                                            sure you are able to keep the vast amount of information we cover organized and memorable.</p>
                                        <p>After attending our four day boot camp class, you will pass the Project Management
                                            Certification Exam on your First Attempt.</p>
                                    </div>
                                </li>
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">What skills will you learn?</a>
                                    <div class="accordion-content" data-tab-content>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </li>
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">Who should take this course?</a>
                                    <div class="accordion-content" data-tab-content>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="certification" style="padding: 1% 0;">
                        <div class="small-12 large-12 columns">
                            <h5>Exam and Certification</h5>
                        </div>
                        <div class="small-12 large-12 columns">
                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                <li class="accordion-item is-active" data-accordion-item>
                                    <a href="#" class="accordion-title">How will I become a PMP certified?</a>
                                    <div class="accordion-content" data-tab-content >
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took</p>
                                    </div>
                                </li>
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">What are the prerequisites for PMP Certification?</a>
                                    <div class="accordion-content" data-tab-content>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </li>
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">What do I need to get my certificate?</a>
                                    <div class="accordion-content" data-tab-content>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="faq" style="padding: 1% 0;">
                        <div class="small-12 large-12 columns">
                            <h5>FAQs</h5>
                        </div>
                        <div class="small-12 large-12 columns">
                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                <li class="accordion-item is-active" data-accordion-item>
                                    <a href="#" class="accordion-title">How will I become a PMP certified?</a>
                                    <div class="accordion-content" data-tab-content >
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took</p>
                                    </div>
                                </li>
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">What are the prerequisites for PMP Certification?</a>
                                    <div class="accordion-content" data-tab-content>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </li>
                                <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">What do I need to get my certificate?</a>
                                    <div class="accordion-content" data-tab-content>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="small-12 large-4 columns">

                </div>


            </div>
        </div>
    </div>
@endsection