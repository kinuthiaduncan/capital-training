@extends('layouts.app')

@section('content')
    <div class="row" style="padding:0;min-width: 100%;">

        <div class="small-12 large-12 columns">
        
            <div class="row course-header">
                 <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row">
                        <div class="small-12 large-12 columns course-title-div">
                        <h2>SIX SIGMA TRAINING COURSE</h2>
                        <p>Our Six Sigma Certification Training Programs and methodology enables students to achieve certification requirements faster than most traditional Six Sigma training programs.Our nationally recognized program offers a variety of options for Six Sigma training and certification classes.Our Six Sigma Training Courses are accredited by the Council for Six Sigma Certification.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div class="small-12 large-12 columns" id="course-advisor">
            <div class="row">
                <div class="small-12 large-10 large-offset-1 columns" style="padding: 2% 5%;">
                    <div class="row" style="padding-bottom: 2%;">
                        <div class="small-12 large-12 columns">
                             <div class="row">
                                 @foreach($courses as $course)
                                    <div class="small-12 large-3 columns sigma-courses">
                                        <div class="">
                                            <i class="fa fa-cloud" style="color: #ccc; font-size: 80px; margin-bottom: 40px;"></i>
                                            <h5>{!! $course->course_name !!}</h5>
                                           {!! $course->description !!}
                                            <a href="{{url('/sigma/'.$course->slug)}}" class="button sigma-btn">BOOK NOW</a>
                                        </div>
                                    </div>
                                 @endforeach
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection