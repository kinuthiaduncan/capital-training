@extends('layouts.mail')

@section('content')
    <p><b>Dear {{$user->first_name}} {{$user->last_name}},</b></p>
    <p>Course Payment Received.</p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <th colspan="2">Payment Details</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Course</b></td><td>{!! $course_type->course()->first()->course_name !!}</td>
        </tr>
        <tr>
            <td><b>Type</b></td><td>{!! $course_type->title !!}</td>
        </tr>
        <tr>
            <td><b>Amount Paid</b></td><td>${!! $request['cost'] !!}</td>
        </tr>
        </tbody>
    </table>
    <br />
    <p class="alert">Course payment completed. We will get back to you</p>
@endsection