@extends('layouts.mail')

@section('content')
    <h1> New Contact Request. The details are: </h1>
    <table class="table table-responsive table-striped">
        <tr>
            <td>Name</td><td>{!! $data['name'] !!}</td>
        </tr>
        <tr>
            <td>Email</td><td>{!! $data['email'] !!}</td>
        </tr>
        <tr>
            <td>Subject</td><td>{!! $data['subject'] !!}</td>
        </tr>
        <tr>
            <td>Message</td><td>{!! $data['message'] !!}</td>
        </tr>

    </table>
@endsection