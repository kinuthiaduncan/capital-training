@extends('layouts.mail')

@section('content')
    <h1>New Booking Request. The details are:</h1>
    <table class="table table-responsive table-striped">
        <tr>
            <td>Date</td><td>{!! $data['date'] !!}</td>
        </tr>
        <tr>
            <td>Time</td><td>{!! $data['time'] !!}</td>
        </tr>
        <tr>
            <td>Name</td><td>{!! $data['name'] !!}</td>
        </tr>
        <tr>
            <td>Email</td><td>{!! $data['email'] !!}</td>
        </tr>
        <tr>
            <td>Subject</td><td>{!! $data['subject'] !!}</td>
        </tr>
        <tr>
            <td>Message</td><td>{!! $data['message'] !!}</td>
        </tr>

    </table>
@endsection