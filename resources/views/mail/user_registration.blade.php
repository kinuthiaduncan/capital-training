@extends('layouts.mail')

@section('content')
    <p><b>Dear {{$user->first_name}} {{$user->last_name}},</b></p>
    <p>This is an email confirmation to your course interest.</p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <th colspan="2">Registration Details</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Course</b></td><td>{!! $course_type->course()->first()->course_name !!}</td>
        </tr>
        <tr>
            <td><b>Type</b></td><td>{!! $course_type->title !!}</td>
        </tr>
        @if(!is_null($course_sub_type))
        <tr>
            <td><b>Sub Type</b></td><td>{!! $course_sub_type->type_name !!}</td>
        </tr>
        @endif
        @if($course_location)
        <tr>
            <td><b>Location</b></td><td>{!! $course_location !!}</td>
        </tr>
        @endif
        <tr>
            <td><b>Price</b></td><td>$
                @if(!is_null($course_sub_type))
                    {!! $course_sub_type->price !!}
                @else
                    {!! $course_type->price !!}
                @endif
            </td>
        </tr>
        <tr>
            <td><b>Promo Code</b></td><td>{!! $course_type->promo_code !!}</td>
        </tr>
        </tbody>
    </table>
    <br />
    <p class="alert">Please proceed to payment to finish your registration</p>
@endsection