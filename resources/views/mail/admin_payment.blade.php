@extends('layouts.mail')

@section('content')
    <p>New Course Payment</p>
    <p>A new course booking payment has been done:</p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <th colspan="2">Payment Details</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Name</b></td><td>{!! $user->first_name !!} {!! $user->last_name !!}</td>
        </tr>
        <tr>
            <td><b>Email</b></td><td>{!! $user->email !!}</td>
        </tr>
        <tr>
            <td><b>Phone</b></td><td>{!! $user->phone !!}</td>
        </tr>
        <tr>
            <td><b>Course</b></td><td>{!! $course_type->course()->first()->course_name !!}</td>
        </tr>
        <tr>
            <td><b>Type</b></td><td>{!! $course_type->title !!}</td>
        </tr>
        <tr>
            <td><b>Amount Paid</b></td><td>${!! $request['cost'] !!}</td>
        </tr>
        <tr>
            <td><b>Promo Code</b></td><td>{!! $course_type->promo_code !!}</td>
        </tr>
        </tbody>
    </table>
    <br />

@endsection