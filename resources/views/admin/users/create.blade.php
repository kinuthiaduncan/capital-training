@extends('layouts.admin')
@section('content')
    <div class="row user-heading">
        <div class="small-12 large-6 columns">
            <h3>Add New User</h3>
        </div>
        <div class="small-12 large-6 columns">
            <a href="{{route('user.index')}}" class="button pull-right admin-btns"><i class="fa fa-arrow-left" aria-hidden="true"></i> BACK</a>
        </div>
    </div>
    <div class="row user-content">
        <div class="small-12 large-8 large-offset-2 columns admin-form">
            {!! Form::open(['route'=>'user.store']) !!}
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', null, ['required', 'placeholder'=>'Enter user\'s name...']) !!}
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', null, ['required', 'placeholder'=>'Enter user\'s email...']) !!}
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password',null,['required', 'placeholder'=>'Enter user\'s password...']) !!}
            {!! Form::label('user_level','User Level') !!}
            {!! Form::select('user_level',['1' => 'Admin', '2' => 'Normal'],['required', 'placeholder'=>'Select User Level']) !!}
            {!! Form::submit('Add User',['class'=>'button admin-form-btns']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection