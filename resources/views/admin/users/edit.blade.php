@extends('layouts.admin')
@section('content')
    <div class="row user-heading">
        <div class="small-12 large-6 columns">
            <h3>Update User Details</h3>
        </div>
        <div class="small-12 large-6 columns">
            <a href="{{route('user.index')}}" class="button pull-right admin-btns"><i class="fa fa-arrow-left" aria-hidden="true"></i> BACK</a>
        </div>
    </div>
    <div class="row user-content">
        <div class="small-12 large-8 large-offset-2 columns admin-form">
            {!! Form::open(['method'=>'PUT','route'=>['user.update',$user->id]]) !!}
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', $user->name, ['required']) !!}
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', $user->email, ['required']) !!}
            {!! Form::label('user_level','User Level') !!}
            {!! Form::select('user_level',['1' => 'Admin', '2' => 'Normal'],$user->user_level,['required']) !!}
            {!! Form::submit('Edit User Details',['class'=>'button admin-form-btns']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection