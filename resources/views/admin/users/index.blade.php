@extends('layouts.admin')
@section('content')
    <div class="row user-heading">
        <div class="small-12 large-6 columns">
            <h3>Users</h3>
        </div>
        <div class="small-12 large-6 columns">
            <a href="{{route('user.create')}}" class="button pull-right admin-btns"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
        </div>
    </div>

    <div class="row user-content">
        <div class="small-12 large-12 tbl-holder columns">
            @if($users)
                <table>
                    <thead>
                    <th>Name</th><th>Email</th><th>User Level</th><th class="action" colspan="2">Action</th>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{!! $user->name !!}</td>
                            <td>{!! $user->email !!}</td>
                            <td>{!! App\Training\Presenters\UserPresenter::presentUserLevel($user->id) !!}</td>
                            <td><a href="{!! route('user.edit',$user->id) !!}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                            <td><a href="{!! url('/user/'.$user->id.'/delete') !!}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5">{!! $users->links() !!}</td>
                    </tr>
                    </tfoot>

                </table>
            @else
            <h5>No users found</h5>

            @endif
        </div>
    </div>

@endsection