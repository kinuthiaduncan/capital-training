@extends('layouts.admin')

@section('content')
    <div class="row user-heading">
        <div class="small-12 large-12 columns">
            <h3>Course Registration / Payments</h3>
        </div>
    </div>
    <div class="row user-content">
        <div class="small-12 large-4 columns">
            {!! Form::open() !!}
            {!! Form::select('course',$courses,null,['v-model'=>'course', 'v-on:change'=>'getPayments()']) !!}
        </div>
        <div class="small-12 large-4 columns">
            {!! Form::select('status',[''=>'Select status','1'=>'Paid','0'=>'Pending'],null,['v-model'=>'status', 'v-on:change'=>'getPayments()']) !!}
            {!! Form::close() !!}
        </div>
        <div class="small-12 large-12 columns">
            <payments></payments>
        </div>
    </div>

@endsection