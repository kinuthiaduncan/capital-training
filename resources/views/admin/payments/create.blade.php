@extends('layouts.admin')

@section('content')
    <div class="row user-heading">
        <div class="small-12 large-6 columns">
            <h3>Record Payments</h3>
        </div>
        <div class="small-12 large-6 columns">
            <a href="{{route('payment.index')}}" class="button pull-right admin-btns">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        </div>
    </div>
    <div class="row user-content">
        <div class="small-12 large-12 columns">
            <table>
                <thead>
                <th colspan="4" style="text-align: center">Registration Details</th>
                </thead>
                <tbody>
                <tr>
                    <th>Name</th><td>{!! $registration->first_name !!} {!! $registration->last_name !!}</td>
                    <th>Email</th><td>{!! $registration->email !!}</td>
                </tr>
                <tr>
                    <th>Phone</th><td>{!! $registration->phone !!}</td>
                    <th>Course</th><td>{!! $registration->course()->first()->course_name !!}</td>
                </tr>
                <tr>
                    <th>Course Type</th>
                    <td>{!! \App\Training\Presenters\CoursePresenter::presentCourseType($registration->course_id,$registration->course_type) !!}</td>
                    <th>Sub Type</th>
                    <td>{!!\App\Training\Presenters\CoursePresenter::presentSubType($registration->course_id,$registration->course_sub_type) !!}</td>
                </tr>
                <tr>
                    <th>Amount Due</th>
                    <td colspan="3" style="font-weight: 600; color: red;">${!! \App\Training\Presenters\CoursePresenter::presentPrice($registration->course_id,$registration->course_type,$registration->course_sub_type) !!}</td>
                </tr>
                </tbody>

            </table>
        </div>
        <div class="small-12 large-8 large-offset-2 columns">
            <br />
            <h6 style="text-align: center">Record Payment for above course registration</h6>
            <br />
            {!! Form::open(['route'=>'payment.store']) !!}
            {!! Form::hidden('course_registration_id',$registration->id,['required']) !!}
            {!! Form::number('amount',null,['required','placeholder'=>'Enter amount paid...']) !!}
            {!! Form::submit('Register Payment',['class'=>'button admin-form-btns']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection