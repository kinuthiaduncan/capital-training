@extends('layouts.admin')

@section('content')
    <div class="row user-heading">
        <div class="small-12 large-12 columns">
            <h3>Course Trainer Management</h3>
        </div>
    </div>
    <div class="row user-content">
        <div class="small-6 large-4 columns">
            {!! Form::open() !!}
            {!! Form::text('trainer',null,['v-model'=>'trainer', 'v-on:keyup'=>'getTrainers()','placeholder'=>'Search trainer by first name...']) !!}
            {!! Form::close() !!}
        </div>
        <div class="small-6 large-8 columns">
            <a href="{{route('trainers.create')}}" class="button pull-right admin-btns"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
        </div>
        <div class="small-12 large-12 columns">
            <trainers></trainers>
        </div>
    </div>
@endsection