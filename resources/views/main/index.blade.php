@extends('layouts.app')

@section('content')
    <div class="home-main">
        <div class="row home-row">
            <div class="image-holder">
                {{--<div class="small-12 large-12 columns">--}}
                    {{--@include('layouts.partials.home-nav')--}}
                {{--</div>--}}
                <div class="small-12 large-10 large-offset-1 columns">
                    <div class="row">
                        <div class="small-12 large-12 columns sub-header">
                            <h1>A Sharp Start to your Career</h1>
                        </div>
                        <div class="small-12 large-12 columns find-title">
                            <h2>Find a Class in your Area</h2>
                        </div>
                        <div class="small-12 large-12 columns find-form" style="padding: 2% 2% 4% 2%">
                            {!! Form::open(['route' => 'course-search']) !!}
                            <div class="row course-search">
                                <div class="small-12 large-5 columns">
                                    {!! Form::select('course', $courses, null, ['placeholder' => 'Select Course...','required']) !!}
                                </div>
                                <div class="small-12 large-5 columns">
                                    {!! Form::select('location', $locations, null, ['placeholder' => 'Select Location...','required']) !!}
                                </div>
                                <div class="small-12 large-2 columns" style="text-align: center;">
                                    {!! Form::submit('Search',['class'=>'button search-btn']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
            {{--<div class="small-12 large-12 columns find-class">--}}
                {{--<div class="find-title">--}}
                    {{--<h2>FIND A CLASS IN YOUR AREA</h2>--}}
                {{--</div>--}}
                {{--<div class="find-form" style="padding: 2% 2% 4% 2%">--}}
                    {{--{!! Form::open(['route' => 'course-search']) !!}--}}
                    {{--<div class="row course-search">--}}
                        {{--<div class="small-12 large-5 columns">--}}
                            {{--{!! Form::select('course', $courses, null, ['placeholder' => 'Select Course...','required']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="small-12 large-5 columns">--}}
                            {{--{!! Form::select('location', $locations, null, ['placeholder' => 'Select Location...','required']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="small-12 large-2 columns" style="text-align: center;">--}}
                            {{--{!! Form::submit('Search',['class'=>'button search-btn']) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--{!! Form::close() !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <div class="row" style="padding: 2%">
            <div class="small-12 large-10 large-offset-1 columns">
                <div class="row advert">
                    <div class="small-12 large-6 columns">
                        <h5>LEAVE A MARK</h5>
                    </div>
                    <div class="small-12 large-6 columns">
                        <p>I'm a paragraph. Click here to add your own text and edit me.
                            It’s easy. Just click “Edit Text” or double click me to add your own content and make changes to the font.
                            I’m a great place for you to tell a story and let your users know a little more about you.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection