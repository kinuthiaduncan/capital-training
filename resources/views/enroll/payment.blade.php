@extends('layouts.app')

@section('content')
    <div class="row" style="padding: 2% 0 0 0;min-width: 100%;">
        <div class="small-12 large-10 large-offset-1 columns" style="padding: 0 5%;">
            <div class="row">
                <div class="small-12 large-12 columns alert alert-info">
                   Amount Due:
                    @if(!is_null($course_sub_type))
                        {!! $course_sub_type->price !!}
                    @else
                        {!! $course_type->price !!}
                    @endif
                </div>
                <div class="small-12 large-12 columns alert alert-warning">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> Please use the options below to select your payment method.
                    Your registration is not complete until payment is received.
                </div>
            </div>
            <div class="row">
                <div class="small-12 large-12 columns" style="padding: 1%;">
                    <h4>Please Select a Payment Method</h4>
                </div>
                <div class="small-12 large-6 columns">
                    <div class="row payment-btns">
                        <div class="small-12 large-4 columns choice-btns">
                            <div>
                                <button class="button"><img src="/assets/img/pay-by-credit-card.png" onclick="hideDiv()"></button>
                            </div>
                        </div>
                        <div class="small-12 large-4 columns choice-btns">
                            <div>
                                <button class="button">Click Here if Paying via Check or Money Order</button>
                            </div>
                        </div>
                        <div class="small-12 large-4 columns choice-btns">
                            <div>
                                <button class="button">Generate a Slip to Submit to Employer</button>
                            </div>
                        </div>
                        <div class="small-12 large-12 columns" id="card-details" style="display: none">
                            <form accept-charset="UTF-8" action="{{route('pay')}}" class="require-validation"
                                  data-cc-on-file="false"
                                  data-stripe-publishable-key="pk_test_foSsFVdScSMercQs38q6MRhL"
                                  id="payment-form" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="course_type" value="{{$course_type->id}}">

                                <input type="hidden" name="user" value="{{$user->id}}">
                                <input type="hidden" name="slug" value="{{$course_type->slug}}">
                                @if(!is_null($course_sub_type))
                                    <input type="hidden" name="course_sub_type" value="{{$course_sub_type->id}}">
                                    <input type="hidden" name="cost" value="{{$course_sub_type->price}}">
                                @else
                                    <input type="hidden" name="cost" value="{{$course_type->price}}">
                                @endif
                                <div class='row'>
                                    <div class='small-12 large-12 columns form-group required'>
                                        <label class='control-label'>Name on Card</label>
                                        <input class='form-control' size='4' type='text'>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='small-12 large-12 columns form-group card required'>
                                        <label class='control-label'>Card Number</label>
                                        <input autocomplete='off' class='form-control card-number' size='20' type='text'>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='small-12 large-4 columns form-group cvc required'>
                                        <label class='control-label'>CVC</label> <input
                                                autocomplete='off' class='form-control card-cvc'
                                                placeholder='ex. 311' size='4' type='text'>
                                    </div>
                                    <div class='small-12 large-4 columns form-group expiration required'>
                                        <label class='control-label'>Expiration</label> <input class='form-control card-expiry-month' placeholder='MM' size='2' type='text'>
                                    </div>
                                    <div class='small-12 large-4 columns form-group expiration required'>
                                        <label class='control-label'> </label>
                                        <input class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text'>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='small-12 large-12 columns'>
                                        <div class='form-control total button total-btn'>
                                            Total: <span class='amount'>$
                                                @if(!is_null($course_sub_type))
                                                    {!! $course_sub_type->price !!}
                                                @else
                                                    {!! $course_type->price !!}
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='small-12 large-12 columns form-group'>
                                        <button class='form-control button' type='submit' style="margin-top: 10px;">Pay »</button>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='small-12 large-12 columns error form-group hide'>
                                        <div class='alert-danger alert'>Please correct the errors and try
                                            again.</div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="small-12 large-12 columns" id="cheque-details">

                        </div>
                        <div class="small-12 large-12 columns" id="slip-details">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection