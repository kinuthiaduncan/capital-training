@extends('layouts.app')

@section('content')
    <div class="row" style="padding: 2% 0 0 0;min-width: 100%;">
        <div class="small-12 large-10 large-offset-1 columns" style="padding: 1% 5%;">
            <div class="row">
                <div class="small-12 large-12 columns enroll-header">
                    <h5>{!! $course->course_name !!} : {!! $course_type->title !!}</h5>
                </div>
                <div class="large-6 small-12 columns enroll-details">
                    <h5>Course Details:</h5>
                    <p><b>Course Title:</b> {!! $course->course_name !!}</p>
                    <p><b>Course Type:</b> {!! $course_type->title !!}</p>
                    @if(!is_null($course_sub_type))
                        <p><b>Sub Type:</b> {!! $course_sub_type->type_name !!}</p>
                    @endif
                    <p><b>Price: </b>
                        $
                        @if(!is_null($course_type->price))
                        {!! $course_type->price !!}
                        @else
                        {!! $course_sub_type->price !!}
                        @endif
                    </p>
                </div>
                <div class="large-6 small-12 columns enroll-form">
                    <h5>Registration Details:</h5>
                    {!! Form::open(['route'=>'register-user']) !!}
                    {!! Form::hidden('course_id',$course->id) !!}
                    {!! Form::hidden('course_type',$course_type->id) !!}
                    @if(!is_null($course_sub_type))
                    {!! Form::hidden('course_sub_type',$course_sub_type->id) !!}
                        @if($course_sub_type->location_type == 'physical')
                            {!! Form::label('location_id', 'Select Location*') !!}
                            {!! Form::select('location_id',$locations,['required']) !!}
                        @endif
                    @endif

                    @if($course_type->category == 'physical')
                    {!! Form::label('location_id', 'Select Location*') !!}
                    {!! Form::select('location_id',$locations,['required']) !!}
                    @endif

                    {!! Form::label('first_name', 'First Name*') !!}
                    {!! Form::text('first_name',null,['required']) !!}
                    {!! Form::label('last_name', 'Last Name*') !!}
                    {!! Form::text('last_name',null,['required']) !!}
                    {!! Form::label('email', 'Email*') !!}
                    {!! Form::email('email',null,['required']) !!}
                    {!! Form::label('phone', 'Phone*') !!}
                    {!! Form::text('phone',null,['required']) !!}
                    {!! Form::label('promo_code', 'Promo Code') !!}
                    {!! Form::text('promo_code',null) !!}
                    {!! Form::submit('Continue',['class'=>'button']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection