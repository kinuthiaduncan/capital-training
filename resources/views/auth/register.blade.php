@extends('layouts.app')

@section('content')
    <div class="row" style="padding: 2%">
        <div class="small-12 large-10 large-offset-1 columns">
            <div class="row">
                <div class="small-12 large-12 columns login-header">
                    <h5>Register</h5>
                </div>

                <div class="small-12 large-6 columns form-holder">

                    <form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Name</label>
                            <div>
                                <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-Mail Address</label>
                            <div>
                                <input id="email" type="email" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div>
                                <input id="password" type="password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div>
                            <label for="password-confirm">Confirm Password</label>
                            <div>
                                <input id="password-confirm" type="password" name="password_confirmation" required>
                            </div>
                        </div>

                        <div style="text-align:center;">
                            <div>
                                <button type="submit" class="button login-btn">
                                    Register
                                </button>
                                <a href="{{ url('/login') }}"><u>Already Registered? Log In Here</u></a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="small-12 large-6 columns button-holder">
                    <div class="inside-holder">
                        <div>
                            <br />
                            <a href="#" class="button fb-button">Log in With Facebook</a>
                        </div>
                        <div>
                            <a href="#" class="button plus-button">Log in With Google +</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
