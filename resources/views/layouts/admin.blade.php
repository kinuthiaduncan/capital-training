<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Capital Training Academy</title>

    <!-- Styles -->
    <title>Capital Training Academy</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>
    <link rel='stylesheet' href='/assets/bower_components/fullcalendar/dist/fullcalendar.css' />
    <link rel="stylesheet" href="/assets/bower_components/timepicker/jquery.timepicker.css" />
</head>
<body>
<div id="app">

    {{--@if(Route::current()->getName() != '/')--}}
    @include('layouts.partials.admin-nav')
    {{--@endif--}}
    <div class="main-container">
        <div class="row" data-equalizer="default" style="min-width: 100%">

                <div class="off-canvas position-left show-for-small-only" id="mobile-nav" data-off-canvas data-equalizer-watch="default">
                    @include('layouts.partials.left-nav')
                </div>

                <div class="small-3 medium-2 large-2 columns  side-nav show-for-medium" data-equalizer-watch="default">
                    @include('layouts.partials.left-nav')
                </div>

                <div class="small-12 medium-10 large-10 columns middle-content" data-equalizer-watch="default" v-cloak>
                    @include('layouts.partials.errors')
                        @include('flash::message')
                        @if ((Session::has('success-message')))
                            <div class="alert alert-success">{{Session::get('success-message') }}</div>
                        @endif
                        @if ((Session::has('fail-message')))
                            <div class="alert alert-danger">{{Session::get('fail-message') }}</div>
                        @endif
                    <div v-cloak>
                        @yield('content')
                    </div>
                    </div>
                </div>
        </div>
    {{--@include('layouts.partials.main-footer')--}}
</div>
</body>
<!-- Scripts -->


<script>
    window.Laravel = {csrfToken: '{{ csrf_token() }}'};
</script>


{{--<script src='/assets/bower_components/jquery/dist/jquery.min.js'></script>--}}

<script src="{{mix('/js/app.js')}}"></script>

<script>
    $(document).foundation();
</script>

</html>
