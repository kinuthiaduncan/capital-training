<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Capital Training Academy</title>

    <!-- Styles -->
    <title>Capital Training Academy</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>
    <link rel='stylesheet' href='/assets/bower_components/fullcalendar/dist/fullcalendar.css' />
    <link rel="stylesheet" href="/assets/bower_components/timepicker/jquery.timepicker.css" />
</head>
<body>
    <div id="app">

        {{--@if(Route::current()->getName() != '/')--}}
            @include('layouts.partials.main-nav')
        {{--@endif--}}

        <div class="web-content" v-cloak>
            @include('flash::message')
            @if ((Session::has('success-message')))
                <div class="alert alert-success">{{
					Session::get('success-message') }}</div>
            @endif @if ((Session::has('fail-message')))
                <div class="alert alert-danger">{{
					Session::get('fail-message') }}</div>
            @endif
            @yield('content')
        </div>

        @include('layouts.partials.main-footer')
    </div>
</body>
    <!-- Scripts -->
    <script>
        window.Laravel = {csrfToken: '{{ csrf_token() }}'};
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC1o8ri0pEVzta3NxZGIyTTbzlXTGLIpk&callback=initMap"></script>
    <script src='/assets/bower_components/jquery/dist/jquery.min.js'></script>
    {{--<script src='/assets/bower_components/moment/min/moment.min.js'></script>--}}
    {{--<script src='/assets/bower_components/fullcalendar/dist/fullcalendar.js'></script>--}}
    {{--<script src="/assets/bower_components/timepicker/jquery.timepicker.js"></script>--}}
    <script src="/js/jqueryScripts.js"></script>
    <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
    <script src="/js/payment.js"></script>


    <script src="{{mix('/js/app.js')}}"></script>

<script>
    $(document).foundation();
</script>

</html>
