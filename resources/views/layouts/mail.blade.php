<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Capital Training Academy</title>
    <style>
        body{
            background-color: rgb(236, 240, 241);
            color: rgb(51, 51, 51);
            display: block;
            font-family: Helvetica Neue, Helvetica, Arial, 'sans-serif';
            font-size: 15px;
            padding: 40px 20px;

        }
        body p, body a, body li, body td{
            font-family: Helvetica Neue, Helvetica, Arial, 'sans-serif';
            font-size: 15px;
        }
        table{
            border-collapse: collapse;
            margin-top:10px;
            margin-bottom:10px;
            width:95% !important;
        }

        .stripe{
            background-color:#e3e3e3 !important;
        }
        .table-responsive{
            width: 100%;
        }
        table tr td, table tr th{
            border:1px solid #D8D8D8;
            padding: 5px;
        }
        table thead tr, table .header td{
            background-color:#000000 !important;
            color: #FFF !important;
            border:1px solid #000000 !important;
        }
        table thead tr td{
            border: solid #000000 1px !important;
        }
        h1{
            color: #000000;
            font-weight: 300;
            font-size: 1.5rem;
            padding: 10px;
        }
        h4{
            color: #000000;
            font-weight: 500;
        }
        .alert{
            color: red;
        }
        .no-underline{
            color: dodgerblue;
            text-decoration: none;
        }
        .heading{
            font-weight: 900;
            font-family: sans-serif;
            font-size: 16px;
            color: #006666;
        }
        .bold{
            font-weight: 700;
            font-family: sans-serif;
            font-size: 14px;
        }
        .email-head{
            height: 100%;
            text-align: center;
        }
        .logo{
            width: 125px;
        }
    </style>
</head>
<body style="padding: 10px;background-color: #E6E6E6;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
<div class="email-body" style="
            padding: 0 0 10px;
            background-clip: content-box;
            background-color: #fff;
            width: 90%;
            margin: auto;">
    <div style="height:5.5em">
        <div class="email-head">
            <img class="logo" src="https://i.imgur.com/T6ptmmv.jpg"/>
        </div>
    </div>
    <div class="email-description" style="padding: 10px;
            line-height: 1.5em;
            font-weight: 300;
             margin:auto;
            width: 90%;
            color: #565656;">

        @yield('content')

        <br/>
        <br/>
        Best regards.
        <br/>
        Capital Training Academy
        <br />
        <br />
    </div>
</div>

</body>
</html>