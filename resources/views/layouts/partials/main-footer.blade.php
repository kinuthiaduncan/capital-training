<div class="row footer-main" style="padding: 2%">
    <div class="small-12 large-10 large-offset-1 columns">
        <div class="row">
            <div class="small-4 large-4 columns">
                <h5>CAPITAL</h5>
                <h6>TRAINING ACADEMY</h6>
            </div>
            <div class="small-4 large-4 columns">
                <p>© {!! \Carbon\Carbon::now()->year !!} by Capital Training Academy</p>
            </div>
            <div class="small-4 large-4 columns" style="text-align: right">
                <p>500 Terry Francois St.</p>
                <p>San Francisco, CA 94158</p>
                <span><a href=""><img src="/assets/img/fb.png"></a></span>
                <span><a href=""><img src="/assets/img/twitter.png"></a></span>
                <span><a href=""><img src="/assets/img/google.png"></a></span>
            </div>
        </div>
        <div class="row">
            <div class="small-12 large-12 columns" style="text-align: center">
                <br />
                <p>PMI, PMBOK, PMP, CAPM, PMI-ACP, PMI-RMP, PMI-SP and the PMI Registered
                    Education Provider logo are registered marks of the Project Management Institute, Inc. </p>
                <p>ITIL® is a Registered Trade Mark of AXELOS Limited.
                    The Swirl logo™ is a trade mark of AXELOS Limited.​</p>
            </div>
        </div>
    </div>
</div>
<div class="row footer-links">
    <div class="small-12 large-12 columns">
        <span><a href="">Terms of Use</a></span>
        <span><a href="">Policies</a></span>
        <span><a href="">Privacy Statement</a></span>
        <span><a href="/about">Contact Us</a></span>
        <span><a href="">Site Map</a></span>
    </div>
</div>

<div style="height: 40px;"></div>

<div class="row footer-fixed">
    <div class="small-12 large-12 columns">
        <span><a href=""><i class="fa fa-question-circle"></i> Help and Support</a></span>
        <span><a href=""><i class="fa fa-mobile"></i> Request a Call</a></span>
        <span><a href=""><i class="fa fa-phone-square"></i> Call us: +1 234 5678</a></span>
        <span><a href="">Site Map</a></span>
    </div>
</div>