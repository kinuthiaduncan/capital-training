<div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
    <div class="row" style="width: 100%">
        <div class="small-3 columns">
            <button class="tm1_menuInitial tm1_dir-center tm1menuButton"
               style="cursor: pointer; margin-top: 0px;" data-toggle>
                <svg preserveAspectRatio="none" viewBox="0 0 17 17" class="tm1_icon tm1iconSVG"
                     data-reactid="133">
                    <line x2="100%" class="tm1_line1 tm1_animating-line"></line>
                    <line x2="100%" class="tm1_line2 tm1_animating-line"></line>
                    <line x2="100%" class="tm1_line3 tm1_animating-line"></line></svg></button>
        </div>
        <div class="small-9 columns">
            <div class="title-bar-title" style="color:red">Capital Training Academy</div>
        </div>
    </div>


</div>

<div class="row">
    <div class="top-bar large-10 large-offset-1 columns" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">
        <div class="row">
            <div class="large-12 small-12 columns">
                <div class="row">
                    <div class="large-2 columns hide-for-small-only">
                        <a href="{{route('/')}}" class="main-logo"><img src="/assets/img/CTAlogo.png"></a>
                    </div>
                    <div class="large-10 small-12 columns">
                        <ul class="dropdown menu home-menu" data-dropdown-menu>
                            <li class="{{ Request::is('/') ? 'current' : '' }}" style="border-right: solid black 1px;">
                                <a href="{{url('/')}}">HOME</a></li>
                            <li class="{{ Request::is('training-courses*') || Request::is('courses*') ? 'current' : '' }}" style="border-right: solid black 1px;">
                                <a href="{{route('courses')}}">TRAINING COURSES</a>
                                {{--<ul class="menu vertical">--}}
                                    {{--@foreach($all_courses as $item)--}}
                                        {{--<li><a href="{{route('training-courses',$item->slug)}}">{!! $item->short_title !!}</a></li>--}}
                                    {{--@endforeach--}}
                                    {{--<li><a href="{{route('training-courses','capm')}}" style="margin-top:2%;">CAPM</a></li>--}}
                                    {{--<li><a href="{{route('training-courses','pmp')}}">PMP</a></li>--}}
                                    {{--<li><a href="{{route('training-courses','agile')}}">AGILE</a></li>--}}
                                    {{--<li><a href="{{route('training-courses','sigma')}}">SIX SIGMA</a></li>--}}
                                {{--</ul>--}}
                            </li>
                            <li class="{{ Request::is('about') ? 'current' : '' }}" style="border-right: solid black 1px;">
                                <a href="{{route('about')}}">ABOUT</a></li>
                            <li class="{{ Request::is('faqs') ? 'current' : '' }}" style="border-right: solid black 1px;"><a href="{{route('faqs')}}">FAQ'S</a></li>
                            <li class="{{ Request::is('student-satisfaction') ? 'current' : '' }}"><a href="{{route('student-satisfaction')}}">STUDENT SATISFACTION POLICY</a></li>
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
