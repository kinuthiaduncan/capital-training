<div class="mynav">
    <ul class="vertical menu" data-accordion-menu>
        <li class="{{ Request::is('home**') ? 'current' : '' }}">
            <a href="/home">
                Dashboard <i class="fa fa-home pull-left" aria-hidden="true"></i>
            </a>
        </li>
        <li class="{{ Request::is('payments*') ? 'current' : '' }}">
            <a  href="{{ route('payment.index') }}">
                Payments <i class="fa fa-briefcase pull-left" aria-hidden="true"></i>
            </a>
        </li>


        <li class="{{ Request::is('user*') ? 'current' : '' }}">
            <a href="{{route('user.index')}}">
                Users <i class="fa fa-users pull-left" aria-hidden="true"></i>
            </a>
        </li>

        <li>
            <a href="#">
                Site Content <i class="fa fa-area-chart pull-left" aria-hidden="true"></i>
            </a>
            <ul class="menu vertical nested"  v-cloak>
                <li class="{{ Request::is('trainers*') ? 'current' : '' }}">
                    <a href="{{route('trainer.index')}}">Trainers</a></li>
                <li><a href="#">Courses</a></li>
                <li><a href="#">Home Page</a></li>
                <li><a href="#">About Page</a></li>
            </ul>
        </li>
        <li>
            <a href="{{url('/')}}" target="_blank">
                Visit Site <i class="fa fa-arrow-right pull-left" aria-hidden="true"></i>
            </a>
        </li>
    </ul>
</div>