@if ($errors->any())
    <div style=" color: red; list-style-type: none">
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    </div>
@endif