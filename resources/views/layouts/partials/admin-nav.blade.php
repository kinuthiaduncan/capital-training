<div class="contain-to-grid" data-sticky-container>
    <div class="top-bar" data-options="marginTop:0;" style="width:100%;padding-bottom: 0;">

        <div class="top-bar-title">
            <ul class="menu">
                <li style="padding-right: 8px;">
                    <a data-toggle="mobile-nav" class="show-for-small-only  menu-icon dark mobile-menu-btn"
                       type="button">
                    </a>
                </li>
                <li class="logo"><a id="logo" href="/login"><img src="/assets/img/CTA_logo.jpg"></a></li>
            </ul>
        </div>

        <div id="responsive-menu" class="top-bar-section" style="width: 22%; height: 100%">
            <div class="top-bar-left my-title">
                <ul class="menu">
                    <li>
                        <span class="project-title"></span>
                    </li>
                </ul>
            </div>

            <div class="top-bar-right row" style="height: 100%" v-cloak>
                <div class="small-12 large-12 column" style="padding-right: 0">
                    <ul class="dropdown menu" style="height: 100%;" data-dropdown-menu>
                        
                        <li class="hide-for-small-only name-holder">
                            <a href="#" class="user-name">{!! Auth::user()->email !!}</a>
                            <ul class="menu vertical">
                                <li><a id="logout" href="/logout">Logout</a></li>
                            </ul>
                        </li>
                        <li class="avatar-holder">
                            @if (Auth::user()->avatar == null)
                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                            @else
                                <img src="{{Auth::user()->avatar}}"
                                     class="profile-pic">
                            @endif
                            <ul class="menu vertical show-for-small-only">
                                <li><a id="logout" href="/logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>