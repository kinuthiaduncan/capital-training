<div style="width: 100%">
	<ul class="course-anchor">
		<li><a href="#course-advisor">Course Advisor</a></li>
		<li><a href="#features">Key features</a></li>
		<li><a href="#description">Course Description</a></li>
		<li><a href="#certification">Exam and Certification</a></li>
		<li><a href="#faq">FAQs</a></li>
	</ul>
</div>