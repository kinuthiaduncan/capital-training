<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Capital Training Academy</title>

        <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>

    </head>
    <body>
    <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
        <button class="menu-icon" type="button" data-toggle></button>
        <div class="title-bar-title">Menu</div>
    </div>


        <div>
            @if (Route::has('login'))
                <div>
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

    </body>
    <script src="{{mix('/js/app.js')}}"></script>

    <script>
        $(document).foundation();

    </script>

</html>
