<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseDetails extends Model
{
    protected $table = 'course_details';
    protected $fillable = ['course_id','description','location_id','duration','start_time','end_time','format','hours'];

    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }
//    public function creator(){
//        return $this->belongsTo(User::class, 'created_by');
//    }
}
