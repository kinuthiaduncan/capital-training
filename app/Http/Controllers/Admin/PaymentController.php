<?php

namespace App\Http\Controllers\Admin;

use App\CourseRegistration;
use App\Payments;
use App\Training\Payments\PaymentFilter;
use App\Training\Payments\PaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    private $paymentRepository;
    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function index()
    {
        $courses = $this->paymentRepository->getCoursesSelect();
        return view('admin.payments.index',['courses'=>$courses]);
    }

    public function getPayments()
    {
        $input = request()->get('input');
        $pagination = request()->get('pagination');

        return PaymentFilter::filter($input, $pagination);
    }

    public function create($id)
    {
        $registration = CourseRegistration::findOrFail($id);
        return view('admin.payments.create',['registration'=>$registration]);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'course_registration_id'=>'required',
            'amount'=>'required'
        ]);
        Payments::create($request->except('_token'));
        CourseRegistration::where('id',$request->course_registration_id)->update(['status'=>1]);

        flash('Payment Updated Successfully')->success();
        return redirect()->route('payment.index');
    }
    
    public function delete($id)
    {
        CourseRegistration::findOrFail($id)->forceDelete();
        flash('Registration Deleted Successfully')->success();
        return redirect()->route('payment.index');
    }
}
