<?php

namespace App\Http\Controllers\Course;

use App\Course;
use App\CourseRegistration;
use App\CourseTypes;
use App\Location;
use App\Mail\AdminPaymentMail;
use App\Mail\CourseRegistrationMail;
use App\Mail\UserCourseRegistrationMail;
use App\Mail\UserPaymentMail;
use App\Payments;
use App\SigmaCourses;
use App\SigmaCourseType;
use App\Training\Course\CourseRepository;
use http\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;

class EnrollController extends Controller
{

    private $courseRepository;

    /**
     * EnrollController constructor.
     * @param CourseRepository $courseRepository
     */
    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug)
    {
        $course_type = CourseTypes::where('slug', $slug)->first();
        $locations = $this->courseRepository->getSpecificLocation($course_type->course_id);
        $course_sub_type = null;
        if($course_type){
            $course = $course_type->course()->first();
            return view('enroll.index', ['course_type'=>$course_type, 'course'=>$course,'course_sub_type'=>$course_sub_type, 'locations'=>$locations]);
        }
        return Redirect::back();
    }

    public function sigmaEnroll($slug)
    {

        $course_sub_type = SigmaCourseType::where('slug',$slug)->first();

        if($course_sub_type)
        {
            $course_type = $course_sub_type->sigmaCourse()->first();
            $course = $course_type->course()->first();
            $locations = $this->courseRepository->getSpecificLocation($course->id);
            return view('enroll.index', ['course_type'=>$course_type, 'course'=>$course,'course_sub_type'=>$course_sub_type, 'locations'=>$locations]);

        }
        return Redirect::back();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'course_id' => 'required',
            'course_type' => 'required',
           'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);

        $user = CourseRegistration::create($request->except('_token'));

        $course_location = NULL;
        $course_sub_type = NULL;

        if($request->course_sub_type)
        {
            $course_type = SigmaCourses::findOrFail($user->course_type);

            $sub_type = $request->course_sub_type;
            $course_sub_type = SigmaCourseType::findOrFail($sub_type);
        }
        else{
            $course_type = CourseTypes::findOrFail($user->course_type);
        }

        if($request->location_id)
        {
            $location = Location::findOrFail($request->location_id);
            $course_location = $location->location_name;
        }

        Mail::to(getenv('SUPPORT_MAIL'))->send(new CourseRegistrationMail($user, $course_type,$course_sub_type, $course_location));
        Mail::to($user->email)->send(new UserCourseRegistrationMail($user, $course_type, $course_sub_type, $course_location));
        flash('Details captured. Proceed to Payments')->success();

       return view('enroll.payment',['user'=>$user, 'course_type'=>$course_type, 'course_sub_type'=>$course_sub_type]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public  function pay(Request $request)
    {
        $this->validate($request, [
            "course_type" => "required",
            "user" => "required",
            "slug" => "required",
            "cost" => "required"
        ]);

        $user = CourseRegistration::findOrFail($request->user);

        Stripe::setApiKey ( getenv('STRIPE_KEY') );

        try {
            Charge::create ( array (
                "amount" => $request->cost,
                "currency" => "usd",
                "source" => $request->input ( 'stripeToken' ), // obtained with Stripe.js
                "description" => "Course Payment"
            ) );

            $payment = Payments::create(['course_registration_id'=>$request->user,'amount'=>$request->cost]);
            $updated = CourseRegistration::where('id',$request->user)->update(['status'=>1]);

            Mail::to(getenv('SUPPORT_MAIL'))->send(new AdminPaymentMail($user,$request->all()));
            Mail::to($user->email)->send(new UserPaymentMail($user, $request->all()));

            Session::flash ( 'success-message', 'Payment done successfully !' );
            return redirect('/courses');

        } catch ( Exception $e ) {
            Session::flash ( 'fail-message', "Error! Please Try again." );
            return redirect('/courses');
        }
    }


}
