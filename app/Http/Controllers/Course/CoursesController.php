<?php

namespace App\Http\Controllers\Course;

use App\Course;
use App\CourseDetails;
use App\CourseTypes;
use App\SigmaCourses;
use App\SigmaCourseType;
use App\Trainers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug)
    {

            $course = Course::where('slug', $slug)->first();
            if ($course) {
                $trainer = Trainers::findOrFail($course->trainer_id);
                $individual_courses = $course->courseTypes()->where('category', 'remote')
                    ->take(2)->get();
                $business_course = $course->courseTypes()->where('category', 'physical')->first();
                $course_features = $course->features()->get();
                $description = $course->descriptions()->get();
                $certifications = $course->certifications()->get();
                $faqs = $course->faqs()->get();
                $sigma_courses = SigmaCourses::all();
                if ($course->slug == 'sigma') {
                    return view('sigma.index',['courses'=>$sigma_courses]);
                }
                return view('course.show', ['course' => $course, 'trainer' => $trainer,
                    'individual_courses' => $individual_courses, 'business_course' => $business_course,
                    'course_features' => $course_features, 'description' => $description,
                    'certifications' => $certifications, 'faqs' => $faqs]);
            }

            return Redirect::back();

    }
    /**
     *
     */
    public function course()
    {
        return view('course.index');
    }


    public function show($slug)
    {
        $sigma_course = SigmaCourses::where('slug', $slug)->first();
        if($sigma_course)
        {
            $course = Course::findOrFail($sigma_course->course_id);
            $trainer = Trainers::findOrFail($course->trainer_id);
            $course_features = $course->features()->get();
            $description = $course->descriptions()->get();
            $certifications = $course->certifications()->get();
            $faqs = $course->faqs()->get();
            $course_types = SigmaCourseType::where('sigma_course_id', $sigma_course->id)->get();

            return view('sigma.show', ['course'=>$course, 'trainer'=>$trainer, 'course_features'=>$course_features,
                            'description'=>$description,'certifications'=>$certifications, 'faqs'=> $faqs, 'course_types'=>$course_types]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        $this->validate($request,[
            'course'=>'required',
            'location'=>'required'
        ]);
        $course_details = CourseDetails::where('course_id',$request->course)
            ->where('location_id',$request->location)->first();

        return view('course.course-details',['course_details'=>$course_details]);
    }
}
