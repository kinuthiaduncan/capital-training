<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Stmt\Return_;

class AboutController extends Controller
{
    public function index()
    {
        return view('about');
    }

    public function contact(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'email|',
            'subject'=>'required',
            'message'=>'required'
        ]);
        $contact_details = Contact::create($request->except('_token'));
        Mail::to(getenv('SUPPORT_MAIL'))->send(new ContactMail($contact_details));
        flash('Message sent successfully!!')->success();
        return Redirect::back();
    }
}
