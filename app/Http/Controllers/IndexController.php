<?php

namespace App\Http\Controllers;

use App\Course;
use App\Training\Course\CourseRepository;
use App\Training\Location\LocationRepository;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    private $locationRepository;
    private $courseRepository;
    public function __construct(CourseRepository $courseRepository, LocationRepository $locationRepository)
    {
        $this->courseRepository = $courseRepository;
        $this->locationRepository = $locationRepository;
    }

    public function index()
    {
        $courses = $this->courseRepository->getCourses();
        $locations = $this->locationRepository->getLocations();
        return view('main.index',['courses'=>$courses, 'locations'=>$locations]);
    }
}
