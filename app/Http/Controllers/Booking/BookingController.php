<?php

namespace App\Http\Controllers\Booking;

use App\Booking;
use App\Mail\BookingMail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('booking.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCalendar()
    {
        return view('booking.calendar');
    }

    public function inputDetails(Request $request)
    {
        $this->validate($request,[
            'date'=>'required',
            'time' => 'required'
        ]);
        $date =$request->date;
        $time = $request->time;
       return view('booking.details',['date'=>$date,'time'=>$time]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "date" => "required",
            "time" => "required",
            "name" => "required",
            "email" => "required"
        ]);
        $booking = Booking::create([
            'date' => Carbon::parse($request->date)->toDateString(),
            'time' => Carbon::parse($request->time)->toTimeString(),
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
        ]);
        Mail::to(getenv('SUPPORT_MAIL'))->send(new BookingMail($booking));
        Mail::to($booking->email)->send(new BookingMail($booking));
        flash('Booking Successful')->success();
        return redirect()->route('calendar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
