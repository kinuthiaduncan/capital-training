<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainers extends Model
{
    protected $table = 'trainers';

    public function course()
    {
        return $this->hasMany(Course::class);
    }

    public function scopeOfTrainer($query, $type)
    {
        if ($type == "") {
            return $query->whereNotNull('first_name');
        }

        return $query->where('first_name', 'LIKE', "%$type%");
    }

}
