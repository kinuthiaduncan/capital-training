<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Features extends Model
{
    protected $table = 'course_features';

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
