<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCourseRegistrationMail extends Mailable
{
    use Queueable, SerializesModels;
    private $user;
    private $course_type;
    private $course_sub_type;
    private $course_location;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $course_type
     * @param $course_sub_type
     * @param $course_location
     */
    public function __construct($user, $course_type,$course_sub_type,$course_location )
    {
        $this->user = $user;
        $this->course_type = $course_type;
        $this->course_sub_type = $course_sub_type;
        $this->course_location = $course_location;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user_registration')
            ->with(['user'=>$this->user,'course_type'=> $this->course_type,'course_sub_type'=>$this->course_sub_type, 'course_location'=>$this->course_location])
            ->from('support@capitaltraining.com')
            ->subject('Course Interest Confirmation');
    }
}
