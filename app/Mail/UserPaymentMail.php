<?php

namespace App\Mail;

use App\CourseTypes;
use App\SigmaCourses;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserPaymentMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $request;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $request
     */
    public function __construct($user, $request)
    {
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(array_key_exists('course_sub_type',$this->request))
        {
            $type = SigmaCourses::findOrFail($this->request['course_type']);
        }
        else{
            $type = CourseTypes::findOrFail($this->request['course_type']);
        }
        return $this->view('mail.user_payment')
            ->with(['user'=>$this->user,'course_type'=> $type, 'request'=>$this->request])
            ->from('support@capitaltraining.com')
            ->subject('Course Payment Confirmation');
    }
}
