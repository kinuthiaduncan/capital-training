<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faqs extends Model
{
    protected $table = 'course_faqs';

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
