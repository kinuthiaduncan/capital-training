<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    protected $table = 'certification_questions';

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
