<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SigmaCourses extends Model
{
    protected $table = 'sigma_courses';

    public function sigmaTypes()
    {
        return $this->hasMany(SigmaCourseType::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }
}
