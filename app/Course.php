<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ['location_name'];

    public function trainers()
    {
        return $this->belongsTo(Trainers::class, 'trainer_id');
    }

    public function courseTypes()
    {
        return $this->hasMany(CourseTypes::class);
    }

    public function sigmaCourse()
    {
        return $this->hasMany(SigmaCourses::class);
    }

    public function features()
    {
        return $this->hasMany(Features::class);
    }

    public  function descriptions()
    {
        return $this->hasMany(CourseDescription::class);
    }

    public function certifications()
    {
        return $this->hasMany(Certification::class);
    }

    public function faqs()
    {
        return $this->hasMany(Faqs::class);
    }
}
