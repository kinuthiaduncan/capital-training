<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseRegistration extends Model
{
    protected $table = 'course_registration';
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'course_type', 'promo_code','course_id','course_sub_type'];


    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function payment()
    {
        return $this->hasMany(Payments::class);
    }

    public function scopeOfCourse($query, $type)
    {
        if ($type == "") {
            return $query->whereNotNull('course_id');
        }

        return $query->where('course_id', 'LIKE', "%$type%");
    }

    public function scopeOfStatus($query, $type)
    {
        if ($type == "") {
            return $query->whereNotNull('status');
        }

        return $query->where('status', 'LIKE', "%$type%");
    }
}
