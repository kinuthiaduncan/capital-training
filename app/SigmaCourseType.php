<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SigmaCourseType extends Model
{
    protected $table = 'sigma_course_types';

    public function sigmaCourse()
    {
        return $this->belongsTo(SigmaCourses::class, 'sigma_course_id');
    }
}
