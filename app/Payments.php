<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = 'payments';
    protected $fillable = ['course_registration_id', 'amount'];

    public function registration()
    {
        return $this->belongsTo(CourseRegistration::class, 'course_registration_id');
    }
}
