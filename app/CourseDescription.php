<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseDescription extends Model
{
    protected $table = 'description_questions';

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
