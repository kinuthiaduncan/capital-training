<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseTypes extends Model
{
    protected $table = 'course_types';

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
