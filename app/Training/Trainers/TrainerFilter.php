<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 3/5/18
 * Time: 4:11 PM
 */

namespace App\Training\Trainers;

use App\Trainers;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class TrainerFilter
{
    use PresentableTrait;


    public static function filter($request, $pagination)
    {
        $query = self::query($request);
        $methods = self::scopes();

        foreach ($request as $key => $param) {

            // If a scope exists in the provided array, add it to the query.
            if (array_key_exists($key, $methods)) {
                switch ($key) {
                    case "trainer":
                        $query->OfTrainer($param);
                        break;
                    default:
                        $query;
                }
            }
        }

        $trainers = $query->get();

        $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
        $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

        $offset = ($page * $perPage) - $perPage;
        $data = $trainers->slice($offset, $perPage, true);
        $trainers = new LengthAwarePaginator($data, $trainers->count(), $perPage, $page);
        $trainers->setPath('/api/get/trainers');

        return $trainers;
    }

    private static function scopes()
    {
        $methods = [
            'trainer' => 'OfTrainer'
        ];

        return $methods;
    }

    private static function query($request)
    {
        $query = Trainers::orderBy('id');

        return $query;
    }
}