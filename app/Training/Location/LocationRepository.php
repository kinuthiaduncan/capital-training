<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 1/16/18
 * Time: 1:09 PM
 */

namespace App\Training\Location;


use App\Location;

class LocationRepository
{
    public function getLocations()
    {
        $locations = Location::all();
        $locations_array = [];
        foreach ($locations as $location)
        {
            $locations_array[ $location->id ]=$location->location_name;
        }

        return $locations_array;
    }
}