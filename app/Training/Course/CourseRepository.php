<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 1/16/18
 * Time: 1:10 PM
 */

namespace App\Training\Course;


use App\Course;
use App\CourseDetails;
use App\Location;

class CourseRepository
{
    public function getCourses()
    {
        $courses = Course::all();
        $courses_array = [];
        foreach ($courses as $course)
        {
            $courses_array[ $course->id ]=$course->course_name;
        }

        return $courses_array;
    }

    public function getSpecificCourses($id)
    {
        $course = Course::findOrFail($id);
        $courses_array = [];
        $courses_array[ $course->id ]=$course->course_name;
        return $courses_array;
    }

    public function getSpecificLocation($id)
    {
        $course_details = CourseDetails::where('course_id', $id)->get();
        $location_array = [];

        foreach ($course_details as $course_detail)
        {
            $location_array[ $course_detail->location_id ]=$this->getLocationName($course_detail->location_id);
        }

        return $location_array;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getLocationName($id)
    {
        $location = Location::findOrFail($id);
        return $location->location_name;
    }
}