<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 2/28/18
 * Time: 3:01 PM
 */

namespace App\Training\Payments;


use App\CourseRegistration;
use App\Payments;
use App\Training\Presenters\CoursePresenter;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class PaymentFilter
{
    use PresentableTrait;

    public static function filter($request, $pagination)
    {
        $query = self::query($request);
        $methods = self::scopes();

        foreach ($request as $key => $param) {

            // If a scope exists in the provided array, add it to the query.
            if (array_key_exists($key, $methods)) {
                switch ($key) {
                    case "status":
                        $query->OfStatus($param);
                        break;
                    case "course":
                        $query->OfCourse($param);
                        break;
                    default:
                        $query;
                }
            }
        }

        $registrations = $query->get();

        $registrations = $registrations->map(function ($registration) {
            $registration->course = $registration->course()->first();
            $registration->type = CoursePresenter::presentCourseType($registration->course->id, $registration->course_type);
            $registration->payment = $registration->payment()->first();
            $registration->payment = !is_null($registration->payment) ? '$'.$registration->payment->amount : 'N/A';
            $registration->status = $registration->status == 1 ? 'Paid' : 'Pending' ;
            return $registration;
        });

        $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
        $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

        $offset = ($page * $perPage) - $perPage;
        $data = $registrations->slice($offset, $perPage, true);
        $registrations = new LengthAwarePaginator($data, $registrations->count(), $perPage, $page);
        $registrations->setPath('/api/get/payments');

        return $registrations;
    }

    private static function scopes()
    {
        $methods = [
            'course' => 'OfCourse',
            'status' => 'OfStatus'
        ];

        return $methods;
    }

    private static function query($request)
    {
        $query = CourseRegistration::orderBy('id');

        return $query;
    }
}