<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 2/27/18
 * Time: 3:47 PM
 */

namespace App\Training\Payments;


use App\Course;

class PaymentRepository
{
    public function getCoursesSelect()
    {
        $courses=Course::all();
        $course_array=array(''=>'Select course');
        foreach ($courses as $course)
        {
            $course_array[ $course->id ]=$course->course_name;
        }

        return $course_array;
    }
}