<?php
/**
 * Created by PhpStorm.
 * User: duncan
 * Date: 2/25/18
 * Time: 3:57 AM
 */
namespace App\Training\Presenters;
use App\User;
use Laracasts\Presenter\Presenter;

class UserPresenter extends Presenter
{

    public static function presentUserLevel($userId)
    {
        $user = User::findOrFail($userId);

        if($user->user_level == 1)
        {
            return 'Admin';
        }
        else{
            return "Normal user";
        }
    }

}