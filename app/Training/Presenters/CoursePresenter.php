<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 3/2/18
 * Time: 12:16 PM
 */

namespace App\Training\Presenters;


use App\Course;
use App\CourseTypes;
use App\SigmaCourses;
use App\SigmaCourseType;
use Laracasts\Presenter\Presenter;

class CoursePresenter extends Presenter
{
    public static function presentCourseType($course_id, $type_id)
    {
        $type = null;
        $course = Course::findOrFail($course_id);
        if($course->slug == 'sigma')
        {
            $type = SigmaCourses::findOrFail($type_id);
        }
        else
        {
            $type = CourseTypes::findOrFail($type_id);
        }
        return $type->title;
    }

    public static function presentSubType($course_id, $subtype_id)
    {
        $sub_type = null;
        $course = Course::findOrFail($course_id);

        if($course->slug == 'sigma')
        {
            $sub_type = SigmaCourseType::findOrFail($subtype_id)->type_name;
        }
        else
        {
            $sub_type = 'N/A';
        }
        return $sub_type;
    }

    public static function presentPrice($course_id, $type_id, $subtype_id)
    {
        $price = null;
        $course = Course::findOrFail($course_id);
        if($course->slug == 'sigma')
        {
            $price = SigmaCourseType::findOrFail($subtype_id)->price;
        }
        else
        {
            $price = CourseTypes::findOrFail($type_id)->price;
        }

        return $price;
    }
}